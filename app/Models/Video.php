<?php 

namespace App\Models;

use DB, Illuminate\Database\Eloquent\Model;

class Video extends Model {

	protected $table = 'videos';

	/**
     * Replace Field
     *
     * @access 	public
     * @param 	
     * @return 	string
     */

	public function replaceField($field, $fields = [])
    {
        if (in_array($field, $fields)) {
            return $fields[$field];
        }

        return $field;
    }

	/**
     * Get List of Videos
     *
     * @access 	public
     * @param 	
     * @return 	json(array)
     */

    public function getVideos($request)
    {
        $users = $this->select(['*']);
        if (!empty($request->search['field'])) {
            $searchField = $request->search['field'];
            $searchValue = $request->search['value'];
            $users->where($searchField, 'like', '%' . $searchValue . '%');
            $users->orderBy('videos.id', 'desc');
        }
        return $users->paginate($request->limit);
    }
    
}