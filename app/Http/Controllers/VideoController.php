<?php 

namespace App\Http\Controllers;

use Validator;
use App\Models\Video;
use Illuminate\Http\Request;

class VideoController extends Controller
{
    /**
     * Get List of Videos
     *
     * @access  public
     * @param   
     * @return  json(array)
     */

    public function getVideos(Request $request, Video $video)
    {
        $videos = $video->getVideos($request);
        return response()->json([
            'status' => 'success',
            'result' => [
                'total' => $videos->total(), 
                'rows' => $videos->items()
            ],
            'messages' => null
        ]);
    }

    /**
     * Get List of Videos Without Any Filters
     *
     * @access  public
     * @param   
     * @return  json(array)
     */

    public function getAllVideos(Request $request, Video $video)
    {
        $videos = Video::all();
        return response()->json([
            'status' => 'success',
            'result' => $videos,
            'messages' => null
        ]);
    }

    /**
     * Get Single Video
     *
     * @access  public
     * @param   
     * @return  json(array)
     */

    public function getSingleVideo(Request $request)
    {
        $rules = [
            'id' => 'required'
        ];

        $validator = Validator::make($request->all(), $rules);
        if (!$validator->fails()) {
            $video = Video::find($request->id);
            return response()->json([
                'status' => 'success',
                'result' => $video,
                'messages' => null
            ]);
        } else {
            return response()->json([
                'status' => 'error',
                'result' => $validator->messages(),
                'messages' => null
            ]);
        }
    }

    /**
     * Create a New Video
     *
     * @access  public
     * @param   
     * @return  json(string)
     */

    public function createVideo(Request $request)
    {
        $rules = [
            'title' => 'required',
            'subtitle' => 'required',
            'link' => 'required'
        ];

        $validator = Validator::make($request->all(), $rules);
        if (!$validator->fails()) {
            $video = new Video;
            $video->title = $request->title;
            $video->subtitle = $request->subtitle;
            $video->link = $request->link;
            $video->save();

            return response()->json([
                'status' => 'success',
                'result' => null,
                'messages' => null
            ]);
        } else {
            return response()->json([
                'status' => 'error',
                'result' => $validator->messages(),
                'messages' => null
            ]);
        }
    }

    /**
     * Update Existing Video
     *
     * @access  public
     * @param   
     * @return  json(string)
     */

    public function updateVideo(Request $request)
    {
        $rules = [
            'id' => 'required',
            'title' => 'required',
            'subtitle' => 'required',
            'link' => 'required'
        ];

        $validator = Validator::make($request->all(), $rules);
        if (!$validator->fails()) {
            $video = Video::find($request->id);
            $video->title = $request->title;
            $video->subtitle = $request->subtitle;
            $video->link = $request->link;
            $video->save();

            return response()->json([
                'status' => 'success',
                'result' => null,
                'messages' => null
            ]);
        } else {
            return response()->json([
                'status' => 'error',
                'result' => $validator->messages(),
                'messages' => null
            ]);
        }
    }

    /**
     * Delete Video
     *
     * @access  public
     * @param   
     * @return  json(string)
     */

    public function deleteVideo(Request $request)
    {
        $rules = [
            'id' => 'required'
        ];

        $validator = Validator::make($request->all(), $rules);
        if (!$validator->fails()) {
            $video = Video::find($request->id); 
            $video->delete();

            return response()->json([
                'status' => 'success',
                'result' => null,
                'messages' => null
            ]);
        } else {
            return response()->json([
                'status' => 'error',
                'result' => $validator->messages(),
                'messages' => null
            ]);
        }
    }
}