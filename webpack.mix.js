let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | App Assets
 |--------------------------------------------------------------------------
 */

mix.scripts([
	'resources/app/assets/plugins/jquery/jquery-2.1.1.min.js',
	'resources/app/assets/plugins/moment/min/moment.min.js',
	'resources/app/assets/plugins/dropzone/dist/dropzone.js',
	'resources/app/assets/plugins/chart.js/dist/Chart.min.js',
	'resources/app/assets/plugins/fullcalendar/dist/fullcalendar.js',
	'resources/app/assets/plugins/bootstrap-validator/dist/validator.min.js',
	'resources/app/assets/plugins/select2/dist/js/select2.full.min.js',
	'resources/app/assets/plugins/ckeditor/ckeditor.js',
	'resources/app/assets/plugins/datatable/media/js/jquery.dataTables.min.js',
	'resources/app/assets/plugins/datatable/media/js/dataTables.bootstrap4.min.js',
	'resources/app/assets/plugins/exort/uploader.min.js',
	'resources/app/assets/plugins/tether/dist/js/tether.min.js',
	'resources/app/assets/plugins/bootstrap/dist/js/bootstrap.min.js',
	'resources/app/assets/plugins/owl-carousel/dist/owl.carousel.min.js',
	'resources/app/assets/plugins/slimscroll/jquery.slimscroll.min.js',
	'resources/app/assets/plugins/nanobar/nanobar.min.js',
	'resources/app/assets/plugins/chart.js/dist/Chart.min.js',
	'resources/app/assets/plugins/jquery-ui/jquery-ui.min.js',
	'resources/app/assets/plugins/bootstrap-daterangepicker/daterangepicker.js'
], 'public/app/js/vendor.js').version();

mix.js('resources/app/app.js', 'public/app/js/assets.js').version();
mix.sass('resources/app/app.scss', 'public/app/css/assets.css').options({
	processCssUrls: true
}).version();

mix.copyDirectory('resources/app/assets/images', 'public/app/images');

/*
 |--------------------------------------------------------------------------
 | Front Assets
 |--------------------------------------------------------------------------
 */

mix.scripts([
	'resources/front/js/jquery.js',
	'resources/front/js/vue.min.js',
	'resources/front/js/popper.js',
	'resources/front/js/bootstrap.js',
	'resources/front/js/owl.carousel.min.js'
], 'public/front/js/vendor.js').version();

mix.styles([
	'resources/front/css/font-awesome.css',
	'resources/front/css/pe-icon-7-stroke.css',
	'resources/front/css/pe-icon-7-filled.css',
	'resources/front/css/helper.css',
	'resources/front/css/bootstrap.css',
	'resources/front/css/owl.carousel.min.css',
	'resources/front/css/owl.theme.default.min.css',
	'resources/front/css/custom.css',
	'resources/front/css/responsive.css'
], 'public/front/css/vendor.css').version();

mix.sass('resources/front/sass/main.scss', 'public/front/css/main.css').options({
	processCssUrls: true
}).version();

mix.copyDirectory('resources/front/images', 'public/front/images');
mix.copyDirectory('resources/front/fonts', 'public/front/fonts');