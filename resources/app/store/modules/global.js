import * as types from '../mutation-types'

const state = {
	activeUser: []
}

// getters
const getters = {
	activeUser: state => state.activeUser
}

// actions
const actions = {
	setActiveUser ({ commit }, user) {
		commit(types.SET_ACTIVE_USER, { user })
		window.axios.defaults.params['token'] = user.token
		$.ajaxSetup({
			data: {
				token: user.token
			}
		})
	}
}

// mutations
const mutations = {
	[types.SET_ACTIVE_USER] (state, { user }) {
		state.activeUser = user
	}
}

export default {
	namespaced: true,
    state,
    getters,
    actions,
    mutations
}