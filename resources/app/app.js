
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

import Vue from 'vue'
import store from './store'
import VueRouter from 'vue-router'

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// Components
import Layout from './components/layout/Layout'
import Login from './components/login/Login'
import Home from './components/home/Home'
import Videos from './components/videos/videos'
import CreateVideoForm from './components/videos/CreateVideoForm'
import UpdateVideoForm from './components/videos/UpdateVideoForm'

Vue.use(VueRouter)

const routes = [
	{
		path: '/',
		component: Layout,
		children: [
			{
				path: '/',
				name: 'home',
				component: Home
			},
			{
				path: 'videos',
				name: 'videos',
				component: Videos
			},
			{
				path: 'create-video-form',
				name: 'create-video-form',
				component: CreateVideoForm
			},
			{
				path: 'update-video-form/:id',
				name: 'update-video-form',
				component: UpdateVideoForm
			}
		]
	},
	{
		path: '/login',
		name: 'login',
		component: Login
	}
]

const router = new VueRouter({
	base: '/admin/',
	mode: 'history',
	routes: routes,
	scrollBehavior(to, from, savedPosition) {
		if (savedPosition) {
			return savedPosition
		} else {
			return { x: 0, y: 0 }
		}
	}
})

router.beforeEach((to, from, next) => {

	// Nanobar
	const nanobar = new Nanobar();
	nanobar.go(30);
	nanobar.go(76);

	setTimeout(() => {
		nanobar.go(100);
	}, 1000)

	// After login
	var afterLogin = [
		'login'
	]

	// Before login
	var beforeLogin = [
		'home',
		'videos',
		'create-video-form',
		'update-video-form'
	]

	if (localStorage.getItem('user') !== null && afterLogin.indexOf(to.name) !== -1) {
		next({ name: 'home' })
	} else if (localStorage.getItem('user') === null && beforeLogin.indexOf(to.name) !== -1) {
		next({ name: 'login' }) 
	} else {
		next()
	}
})

const app = new Vue({
	store,
	router
}).$mount('#main')