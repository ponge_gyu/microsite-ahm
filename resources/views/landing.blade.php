<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="icon" type="image/png" href="{{ asset('images/favicon.png') }}">
        <title>AHM - Astra Honda Motor</title>
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="author" content="">
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Template CSS Files -->
        <link rel="stylesheet" href="{{ mix('front/css/vendor.css') }}"/>
        <link rel="stylesheet" href="{{ mix('front/css/main.css') }}"/>
    </head>
    <body>
        <div id="landing">
            <header>
                <div class="container">
                    <nav class="navbar navbar-expand-lg navbar-dark">
                        <a class="navbar-brand" href="#">
                            <img src="{{ asset('front/images/logo-white.png') }}" class="img-fluid">
                        </a>
                        <button class="navbar-toggler" type="button">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        <div class="nav-right ml-auto">
                            <!--BARU DI TAMBAHKAN-->
                            <div class="close_button">
                                <a href="#" class="btn btn-nav-close">&times;</a>
                            </div>
                            <!--END BARU TAMBAH-->
                            <div class="nav-social_wrapper text-right">
                                <a class="nav-social mr-2" href="https://www.facebook.com/welovehonda">
                                    <i class="fa fa-facebook" aria-hidden="true"></i>
                                </a>
                                <a class="nav-social mr-2" href="https://www.twitter.com/welovehonda">
                                    <i class="fa fa-twitter" aria-hidden="true"></i>
                                </a>
                                <a class="nav-social" href="https://www.instagram.com/welovehonda_id">
                                    <i class="fa fa-instagram" aria-hidden="true"></i>
                                </a>
                            </div>
                            <div class="collapse navbar-collapse main-nav" id="navbarSupportedContent">
                                <ul class="navbar-nav">
                                    <li class="nav-item">
                                        <a class="nav-link red" href="javascript:;" id="video-one-heart">GALERI VIDEO</a>
                                    </li>
                                    <!-- <li class="nav-item">
                                        <a class="nav-link red" target="blank" href="https://beatyourlimit.id">DUKUNG ATLET</a>
                                    </li> -->
                                    <li class="nav-item">
                                        <a class="nav-link white" href="javascript:;" id="cara-ikutan">CARA IKUTAN</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link white" href="javascript:;" id="syarat-ketentuan">SYARAT & KETENTUAN</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </nav>
                </div>
            </header>
            <section class="banner_top">
                <div class="owl-carousel owl-theme owl-top">
                    <div class="item">
                        <div class="bg-image content_wrapper banner-1" style="background-image:url('{{ asset('front/images/banner-top-3.jpg') }}');">
                            <div class="container">
                                <div class="left content">
                                    <div class="logo_wrapper">
                                        <img src="{{ asset('front/images/punyacerita-big.png') }}" class="img-fluid">
                                    </div>
                                    <div class="story_wrapper">
                                        <p>
                                            "Filosofi Total Control dari Honda CBR250RR sangat mempengaruhi bagaimana saya dalam menjalani keseharian. Keseimbangan antara power dan manuver di Filosofi Honda CBR250RR saya aplikasikan dalam kehidupan sehari-hari pada perkerjaan dan menjadi bagian teman-teman<br> komunitas. Seimbang di karir, aktif di komunitas dan <br>pegang kendali penuhnya"
                                        </p>
                                        <div class="story_by">
                                            - Rudi, 30 Tahun -
                                        </div>
                                    </div>
                                    <div class="bottom content_bottom">
                                        <p>Ini momen Rudi bersama Honda,
                                            apa momen Honda Brosis?</p>
                                        <!-- <div class="btn_wrapper">
                                            <a href="#" class="btn btn-white">CERITAKAN DISINI</a>
                                        </div> -->
                                    </div><!--END BOTTOM-->
                                </div><!--END CONTENT-->
                            </div><!--END CONTAINER-->
                        </div>
                    </div><!--END ITEM-->
                    <div class="item">
                        <div class="bg-image content_wrapper" style="background-image:url('{{ asset('front/images/banner-top-1.jpg') }}');">
                            <div class="container">
                                <div class="left content">
                                    <div class="logo_wrapper">
                                        <img src="{{ asset('front/images/punyacerita-big.png') }}" class="img-fluid">
                                    </div>
                                    <div class="story_wrapper">
                                        <p>
                                            "Fotografi buatku bukan sekedar hobi, ia adalah passion.
                                            Hal penting dari passion ini adalah memastikan kita tidak ketinggalan momen, 
                                            <b>motor Honda ku memastikan itu</b>"
                                        </p>
                                        <div class="story_by">
                                            - Ade, 24 Tahun -
                                        </div>
                                    </div>
                                    <div class="bottom content_bottom">
                                        <p>Ini momen Ade bersama Honda,
                                            apa momen Honda Brosis?</p>
                                        <!-- <div class="btn_wrapper">
                                            <a href="#" class="btn btn-white">CERITAKAN DISINI</a>
                                        </div> -->
                                    </div><!--END BOTTOM-->
                                </div><!--END CONTENT-->
                            </div><!--END CONTAINER-->
                        </div>
                    </div><!--END ITEM-->
                    <div class="item">
                        <div class="bg-image content_wrapper" style="background-image:url('{{ asset('front/images/banner-top-2.jpg') }}');">
                            <div class="container">
                                <div class="left content">
                                    <div class="logo_wrapper">
                                        <img src="{{ asset('front/images/punyacerita-big.png') }}" class="img-fluid">
                                    </div>
                                    <div class="story_wrapper">
                                        <p>
                                            "Disiplin diri dan waktu penting bagi pemain basket. Menempa diri dengan latihan rutin dan ketat, demi menjadi lebih baik di masa depan. Datang tepat waktu itu telat, datang lebih awal itu tepat waktu. <b>Motor Honda gw men-support itu</b>"
                                        </p>
                                        <div class="story_by">
                                            - Vicky, 19 Tahun -
                                        </div>
                                    </div>
                                    <div class="bottom content_bottom">
                                        <p>Ini momen Vicky bersama Honda,
                                            apa momen Honda Brosis?</p>
                                        <!-- <div class="btn_wrapper">
                                            <a href="#" class="btn btn-white">CERITAKAN DISINI</a>
                                        </div> -->
                                    </div><!--END BOTTOM-->
                                </div><!--END CONTENT-->
                            </div><!--END CONTAINER-->
                        </div>
                    </div><!--END ITEM-->
                </div>
            </section>
            <section class="story-banner">
                <div class="container">
                    <div class="content">
                        <div class="main-title white">
                            Cerita Tentang Aku, Kamu, dan Motor Honda Kita
                        </div>
                        <div class="description white">
                            Honda memahami bahwa motor bukan sekedar solusi
                            transportasi, melainkan juga cermin untuk mencapai impian
                            para pengendara dan khususnya membangun mimpi
                            masyarakat Indonesia.<br><br>
                            Di setiap momen, Honda ingin selalu ada di samping siapapun
                            dan mendukung mereka yang terus berusaha mencapai
                            cita-cita dan mimpi mereka.<br><br>
                            Mari satuHATI berbagi cerita kepada dunia, bagaimana kamu
                            berusaha dan mengejar meraih mimpi bersama motor Honda.
                        </div>
                    </div>
                </div>
            </section>
            <section class="main-video">
                <div class="container">
                    <div class="content">
                        <div class="main-title text-center white">
                            One Heart - Live Your Dream!
                        </div>
                        <iframe src="https://www.youtube.com/embed/h_cPdahJqvM"></iframe>
                        <div class="subtitle text-center">Semangat One Heart</div>
                        <div class="description text-center">
                            Bagi Honda, mimpi adalah kekuatan, kebahagiaan dan sukacita. Mimpi adalah energi positif yang harus kita punya, yang membantu meningkatkan kualitas hidup, itulah kekuatan mimpi. Untuk itu Honda mendorong generasi muda untuk menggapai semua mimpi-mimpinya, dengan semangat One HEART.
                        </div>
                    </div>
                </div>
            </section>
            <!-- <section class="video-list">
                <div class="container">
                    <div class="content">
                        <div class="main-title white">
                            Cerita Kita
                        </div>
                        <div class="row">
                            <div v-for="video in videos.rows" class="col-md-3">
                                <div @click="viewVideo(video)" class="video">
                                    <iframe :src="video.link"></iframe>
                                    <div class="title">{( video.title )}</div>
                                    <div class="info">{( video.subtitle )}</div>
                                </div>
                            </div>
                        </div>
                        <div class="bullets">
                            <div v-for="(bullet, key) in totalVideo" @click="changePage(key)" :class="{ 'bullet': true, 'active': page == (key + 1) ? true : false }"></div>
                        </div>
                        <div class="modal fade" id="modal-video" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">{( modalVideo.title )}</h5>
                                        <button type="button" class="close text-center" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <p>{( modalVideo.subtitle )}</p>
                                        <iframe class="preview" :src="modalVideo.link"></iframe>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section> -->
            <section class="reward">
                <div class="container">
                    <div class="content">
                        <div class="main-title">
                            Dapetin Reward Keren untuk Cerita Terbaik
                        </div>
                        <div class="description">Dapatkan hadiah-hadiah keren di tiap periodenya, ada Drone, Smartphone, Action Camera, dan Voucher belanja. Serta ada hadiah utama All New Honda Vario di akhir periodenya. Makanya submit cerita tentang motor Honda Brosis sekarang juga, siapa tau bisa jadi salah satu yang terpilih untuk mendapatkan hadiah-hadiah tersebut.</div>
                        <!-- <div class="reward-1">
                            <div class="no">1</div>
                            <div class="text">HADIAH UTAMA</div>
                            <img src="{{ asset('front/images/vario.jpg') }}">
                        </div> -->
                        <div class="reward-section-4">
                            <div class="period">
                                <div class="text">PERIODE <span>1</span></div>
                                <div class="month">(AGUSTUS - SEPTEMBER)</div>
                            </div>
                            <div class="reward-11">
                                <div class="no">5</div>
                                <div class="text">VOUCHER MAP</div>
                                <img src="{{ asset('front/images/voucher-2.jpg') }}">
                            </div>
                            <div class="reward-12">
                                <div class="no">1</div>
                                <div class="text">XIAOMI REDMI 5 PLUS</div>
                                <img src="{{ asset('front/images/xiaomi.jpg') }}">
                            </div>
                            <div class="reward-13">
                                <div class="no">5</div>
                                <div class="text">HAND GLOVE</div>
                                <img src="{{ asset('front/images/hand-glove.jpg') }}">
                            </div>
                            <div class="reward-14">
                                <div class="no">5</div>
                                <div class="text">HELM</div>
                                <img src="{{ asset('front/images/helm.jpg') }}">
                            </div>
                        </div>
                        <!-- <div class="reward-section-5">
                            <div class="period">
                                <div class="text">PERIODE <span>2</span></div>
                                <div class="month">(OKTOBER - NOVEMBER)</div>
                            </div>
                            <div class="reward-15">
                                <div class="no">5</div>
                                <div class="text">VOUCHER MAP</div>
                                <img src="{{ asset('front/images/voucher-2.jpg') }}">
                            </div>
                            <div class="reward-16">
                                <div class="no">3</div>
                                <div class="text">JACKET</div>
                                <img src="{{ asset('front/images/jacket.jpg') }}">
                            </div>
                            <div class="reward-17">
                                <div class="no">1</div>
                                <div class="text">DRONE UNIT</div>
                                <img src="{{ asset('front/images/drone-2.jpg') }}">
                            </div>
                            <div class="reward-18">
                                <div class="no">1</div>
                                <div class="text">ACTION CAM</div>
                                <img src="{{ asset('front/images/action-cam.jpg') }}">
                            </div>
                        </div> -->
                        <!-- <div class="reward-section-1">
                            <div class="period">
                                <div class="text">PERIODE <span>1</span></div>
                                <div class="month">(JULI - AGUSTUS)</div>
                            </div>
                            <div class="reward-2">
                                <div class="no">1</div>
                                <div class="text">SMART PHONE</div>
                                <img src="{{ asset('front/images/iphone.png') }}">
                            </div>
                            <div class="reward-3">
                                <div class="no">5</div>
                                <div class="text">VOUCHER MAP</div>
                                <img src="{{ asset('front/images/voucher.png') }}">
                            </div>
                        </div>
                        <div class="reward-section-2">
                            <div class="period">
                                <div class="text">PERIODE <span>2</span></div>
                                <div class="month">(SEPTEMBER - OKTOBER)</div>
                            </div>
                            <div class="reward-4">
                                <div class="no">1</div>
                                <div class="text">ACTION CAM</div>
                                <img src="{{ asset('front/images/camera.png') }}">
                            </div>
                            <div class="reward-5">
                                <div class="no">1</div>
                                <div class="text">SMART PHONE</div>
                                <img src="{{ asset('front/images/iphone.png') }}">
                            </div>
                            <div class="reward-6">
                                <div class="no">5</div>
                                <div class="text">VOUCHER MAP</div>
                                <img src="{{ asset('front/images/voucher.png') }}">
                            </div>
                        </div>
                        <div class="reward-section-3">
                            <div class="period">
                                <div class="text">PERIODE <span>3</span></div>
                                <div class="month">(NOVEMBER - DESEMBER)</div>
                            </div>
                            <div class="reward-7">
                                <div class="no">1</div>
                                <div class="text">DRONE UNIT</div>
                                <img src="{{ asset('front/images/drone.png') }}">
                            </div>
                            <div class="reward-8">
                                <div class="no">1</div>
                                <div class="text">ACTION CAM</div>
                                <img src="{{ asset('front/images/camera.png') }}">
                            </div>
                            <div class="reward-9">
                                <div class="no">1</div>
                                <div class="text">SMART PHONE</div>
                                <img src="{{ asset('front/images/iphone.png') }}">
                            </div>
                            <div class="reward-10">
                                <div class="no">5</div>
                                <div class="text">VOUCHER MAP</div>
                                <img src="{{ asset('front/images/voucher.png') }}">
                            </div>
                        </div> -->
                    </div>
                </div>
            </section>
            <section class="cara_ikutan">
                <div class="container">
                    <div class="main-title text-center white">
                        Cara Ikutan
                    </div>
                    <div class="content">
                        <div class="row">
                            <div class="col-12 col-lg-3 col-sm-6 col-cara">
                                <div class="subtitle">
                                    <div class="top">
                                        <div class="number">1</div>
                                        <div class="icon"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i></div>
                                    </div>
                                    <div class="bottom">
                                        LIKE & FOLLOW
                                    </div>
                                </div>
                                <div class="description">
                                    Like dan Follow semua akun social media Welovehonda Indonesia
                                    <div class="social-media">
                                        <a class="nav-social" href=""><i class="fa fa-facebook"></i></a> Welovehonda Indonesia<br>
                                        <a class="nav-social" href=""><i class="fa fa-instagram"></i></a> @welovehonda_id<br>
                                        <a class="nav-social" href=""><i class="fa fa-twitter"></i></a> @welovehonda<br>
                                    </div>
                                </div>
                            </div><!--END COL-->
                            <div class="col-12 col-lg-3 col-sm-6 col-cara">
                                <div class="subtitle">
                                    <div class="top align-items-end">
                                        <div class="number">2</div>
                                        <div class="icon"><i class="fa fa-upload" aria-hidden="true"></i></div>
                                    </div>
                                    <div class="bottom">
                                        UPLOAD
                                    </div>
                                </div>
                                <div class="description">
                                    Upload foto atau video cerita Bersama motor Honda versi kamu di akun Facebook, Twitter, atau Instagram Brosis
                                </div>
                            </div><!--END COL-->
                            <div class="col-12 col-lg-3 col-sm-6 col-cara">
                                <div class="subtitle">
                                    <div class="top">
                                        <div class="number">3</div>
                                        <div class="icon"><img src="{{ asset('front/images/icon/add-people.png') }}" class="img-fluid"></div>
                                    </div>
                                    <div class="bottom">
                                        AJAK </br> TEMAN
                                    </div>
                                </div>
                                <div class="description">
                                    Mention Welovehonda, dan tag 5 teman Honda Lovers
                                </div>
                            </div><!--END COL-->
                            <div class="col-12 col-lg-3 col-sm-6 col-cara">
                                <div class="subtitle">
                                    <div class="top">
                                        <div class="number">4</div>
                                        <div class="icon"><img src="{{ asset('front/images/icon/hashtag.png') }}" class="img-fluid"></div>
                                    </div>
                                    <div class="bottom">
                                        SERTAKAN </br> HASHTAG
                                    </div>
                                </div>
                                <div class="description">
                                    Jangan lupa sertakan hashtag <strong>#WelovehondaPunyaCerita</strong>
                                </div>
                            </div><!--END COL-->
                        </div><!--END ROW-->
                        <div class="syarat_ketentuan text-center">
                            <div class="logo_wrapper">
                                <img src="{{ asset('front/images/punyacerita-shadow.png') }}" class="img-fluid">
                            </div>
                            <div class="btn_wrapper">
                                <div class="modal fade" id="modal-syarat-ketentuan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">WELOVEHONDA PUNYA CERITA</h5>
                                                <button type="button" class="close text-center" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <p><strong>SYARAT &amp; KETENTUAN “WeloveHonda Punya Cerita”</strong></p>

                                                <p>Welovehonda Punya Cerita adalah program milik PT Astra Honda Motor.<br>
                                                Program ini berlangsung dari bulan Agustus – September 2018.<br>
                                                Pogram ini mengajak Brosis untuk mengikuti bercerita menceritakan pengalamanmeraih mimpinya bersama motor Honda<br>
                                                Sebelum mendaftar, dimohon untuk membaca ketentuan program ini secara seksama.<br>
                                                Dengan mengikuti program ini, Brosis dianggap telah setuju terhadap semua syarat &amp; ketentuan yang berlaku.</p><br>

                                                <p><strong>1. KRITERIA PESERTA</strong></p>
                                                <p>Pria dan wanita berusia minimal 17 tahun pada saat program berlangsung.<br>
                                                Warga negara Indonesia dan berdomisili di Indonesia.<br>
                                                Memiliki akun Facebook, Twitter dan Instagram (tidak berstatus privat).<br>
                                                Sudah me-likes/mem-follow fanpages akun Welovehonda Indonesia dan/atau akun Twitter @Welovehonda dan/atau akun<br>
                                                Instagram @Welovehonda_id dan me-subscribe channel Youtube Welevohonda Indonesia</p><br>

                                                <p><strong>2. PERIODE KOMPETISI</strong></p>
                                                <p>Welovehonda Punya Cerita ini dilaksanakan pada bulan Agustus – September 2018.</p><br>

                                                <p><strong>3. MEKANISME KOMPETISI</strong></p>
                                                <p>Ikuti langkah-langkah berikut untuk berpartisipasi dalam program Welovehonda Punya Cerita:<br>
                                                - Like dan Follow semua akun social media Welovehonda Indonesia<br>
                                                Facebook, Youtube: Welovehonda Indonesia<br>
                                                Instagram: welovehonda_id<br>
                                                Twitter: welovehonda<br>
                                                - Buka laman www.welovehondapunyacerita.com<br>
                                                - Upload foto atau video cerita meraih mimpi bersama motor Honda versi kamu Brosis<br><br>

                                                FACEBOOK<br>
                                                - Upload foto/video Brosis + caption bercerita tentang bagaimana Brosis meraih mimpi bersama motor Honda Brosis + mention Welovehonda Indonesia + hashtag #WelovehondaPunyaCerita + tag 5 teman Honda Lovers Brosis untuk ikutan<br><br>

                                                TWITTER<br>
                                                - Upload foto/video Brosis + caption bercerita tentang bagaimana Brosis meraih mimpi bersama motor Honda Brosis + mention Welovehonda Indonesia + hashtag #WelovehondaPunyaCerita + mention 5 teman Honda Lovers Brosis untuk ikutan<br><br>

                                                INSTAGRAM<br>
                                                - Upload foto/video Brosis + caption bercerita tentang bagaimana Brosis meraih mimpi bersama motor Honda Brosis + mention Welovehonda Indonesia + hashtag #WelovehondaPunyaCerita + tag 5 teman Honda Lovers Brosis untuk ikutan<br>
                                                - Brosis diperbolehkan mengirim lebih dari 1x<br>
                                                - Tidak diwajibkan upload di semua sosial media Brosis<br>
                                                - Pihak penyelenggara akan mengumumkan pemenang pada bulan Oktober melalui situs www.astra-honda.com dan Facebook<br>
                                                Page Welovehonda Indonesia Twitter @Welovehonda dan Instagram @Welovehonda_id</p><br>

                                                <p><strong>4. KETENTUAN TAMBAHAN UNTUK PESERTA KOMPETISI</strong></p>
                                                <p>Peserta dapat mengikuti tantangan selama 2018 berlangsung sesuai dengan petunjuk yang akan diberikan.</p><br>

                                                <p><strong>5. KETENTUAN KOMPETISI</strong></p>
                                                <p>Ketentuan FOTO Welovehonda Punya Cerita :<br>
                                                1. Karya bebas &amp; orisinil (bukan hasil plagiatisme)<br>
                                                2. Karya baru, bukan materi yang pernah dilombakan sebelumnya dan juga tidak melanggar Hak Cipta apapun<br>
                                                3. Karya foto bisa berupa selfie atau wefie yang menampilkan motor Honda beserta aksesoris resminya<br>
                                                4. Perserta diperbolehkan mengirim karya lebih dari 1<br>
                                                5. Editing yang diperbolehkan : Contrast, cropping, dust removing<br>
                                                6. Pengambilan gambar/karya dilakukan di wilayah Negara Kesatuan Republik Indonesia<br>
                                                7. Dengan keikutsertaan, peserta dianggap telah menerima dan menyetujui semua persyaratan<br>
                                                8. Dengan keikutsertaan, peserta menyetujui karyanya menjadi hak milik penyelenggara, PT Astra Honda Motor<br>
                                                9. Peserta dihimbau untuk tidak posting/menyebar materi/foto/video berbau atau bertema SARA, provokatif dan pornografi<br>
                                                10. Peserta dilarang memposting materi merk brand/produk lain di luar persyaratan<br>
                                                Ketentuan Video Welovehonda Punya Cerita:<br>
                                                1. Karya bebas &amp; orisinil (bukan hasil plagiatisme)<br>
                                                2. Karya baru, bukan materi yang pernah dilombakan sebelumnya dan juga tidak melanggar Hak Cipta apapun, terutama audio
                                                copyright.<br>
                                                3. Karya video menceritakan pengalaman meraih mimpi bersama motor Honda<br>
                                                4. Karya berdurasi min 30s dan max 60s<br>
                                                5. Perserta diperbolehkan mengirim video/karya lebih dari 1<br>
                                                6. Peserta diperbolehkan menggunakan software video editing apapun<br>
                                                7. Editing yang diperbolehkan:Cut to cut, menambah backsound, moving objects<br>
                                                8. Pengambilan gambar/karya dilakukan di wilayah Negara Kesatuan Republik Indonesia<br>
                                                9. Dengan keikutsertaan, peserta dianggap telah menerima dan menyetujui semua persyaratan<br>
                                                10. Dengan keikutsertaan, peserta menyetujui karyanya menjadi hak milik penyelenggara<br>
                                                11. Peserta dihimbau untuk tidak posting/menyebar materi/foto/video berbau atau bertema SARA, provokatif dan pornografi<br>
                                                12. Peserta dilarang memposting materi merk brand/produk lain di luar persyaratan
                                                Konten dilarang keras mengandung SARA, provokasi, pornografi dan tidak menampilkan muatan merk atau brand selain produk
                                                dari PT Astra Honda Motor.<br>
                                                Program ini hanya berlaku bagi WNI (Warga Negara Indonesia)<br>
                                                Pihak penyelenggara berhak mendiskualifikasi peserta yang melanggar syarat dan ketentuan lainnya.</p><br>

                                                <p><strong>6. KETENTUAN PEMENANG WELOVEHONDA PUNYA CERITA</strong></p>
                                                <p>Pemenang dipilih berdasarkan system penjurian internal PT Astra Honda Motor dan pihak-pihak yang berkerjasama.<br>
                                                Penentuan pemenang bersifat mutlak dan tidak dapat diganggu gugat.<br>
                                                Pengumuman resmi dilakukan dan ada di website resmi PT Astra Honda Motor dan social media resmi PT Astra Honda Motor.<br>
                                                Hati-hati penipuan yang mengatasnamakan Pihak Penyelenggara dan/atau PT. Astra Honda Motor.</p><br>

                                                <p><strong>7. KETENTUAN HADIAH</strong></p>
                                                <p>1. Ada 14 pemenang terpilih oleh juri dengan Hadiah Utama : 1 unit Smartphone, 5 unit Hand Glove, 3 unit Helm dan 5 voucher MAP @Rp 200.000 untuk 5 pemenang.<br>
                                                2. Gambar hadiah yang tertera di website hanya ilustrasi, hadiah yang diterima dapat mengalami perubahan warna atau type.<br>
                                                3. Peserta tidak diperbolehkan untuk melakukan pemilihan hadiah dari segi bentuk, tipe, atau warna apapun.<br>
                                                4. Hadiah tidak dapat diuangkan atau di pindahtangankan.<br>
                                                5. Pajak hadiah ditanggung pemenang.<br>
                                                6. Konfirmasi alamat lengkap akan dilakukan lewat email, tlp/SMS untuk keperluan pengiriman hadiah.<br>
                                                7. Hadiah akan dikirimkan ke alamat bersangkutan sesuai dengan alamat yang telah diberikan saat konfirmasi pemenang oleh panitia penyelengara.
                                                Pihak Penyelengara tidak bertanggung jawab apabila hadiah yang telah berhasil dikirimkan tidak berhasil mancapai alamat pemenang.
                                                Dalam hal ini dan panitia penyelengara dan/atau PT. Astra Honda Motor akan dibebaskan dari tuntutan apapun sehubungan dangan hal-hal di atas</p>

                                                <p><strong>8. SYARAT &amp; KETENTUAN UMUM</strong></p>
                                                <p>Pihak Penyelenggara berhak mendiskualifikasi peserta yang melanggar syarat dan ketentuan dalam program Welovehonda Punya
                                                Cerita<br>
                                                Pihak Penyelenggara berhak untuk memperbaharui hadiah dari kompetisi ini sewaktu-waktu.<br>
                                                Pihak Penyelenggara berhak untuk melakukan penambahan atau pengurangan syarat dan ketentuan yang berlaku tanpa
                                                melakukan pemberitahuan terlebih dahulu.<br>
                                                Keikutsertaan dalam program promosi ini tidak dipungut biaya.Hati-hati penipuan yang mengatasnamakan Pihak Penyelenggara
                                                dan/atau PT Astra Honda Motor.<br>
                                                Informasi atau pertanyaan lebih lanjut mengenai syarat dan ketentuan ini dapat diajukan ke layanan konsumen PT Astra Honda
                                                Motor. Melalui akun sosial media Welovehonda Indonesia.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <a data-toggle="modal" data-target="#modal-syarat-ketentuan" class="btn btn-black">
                                    <span>KLIK DI SINI UNTUK INFO</span>
                                    <h3>SYARAT & KETENTUAN</h3>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <footer>
                <div class="container">
                    <div class="d-flex">
                        <div class="copyright_wrapper mr-auto">
                             © Copyright 2018 - WeLoveHondaPunyaCerita.com. All rights reserved.
                        </div>
                        <div class="footer_menu_wrapper ml-auto">
                            <a href="#" class="footer_menu mr-2">Privacy Policy</a>
                            <a href="#" class="footer_menu">Terms & Condition</a>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
        <script type="text/javascript" src="{{ mix('front/js/vendor.js') }}"></script>
        <script>
            var landing = new Vue({
                el: '#landing',
                delimiters: ['{(', ')}'],
                data: {
                    page: 1,
                    videos: [],
                    modalVideo: []
                },
                computed: {
                    totalVideo() {
                        if (this.videos.total) {
                            return Math.ceil(this.videos.total / 12)
                        }
                    }
                },
                methods: {
                    viewVideo(video) {
                        this.modalVideo = video
                        $('#modal-video').modal('show')
                    },
                    changePage(page) {
                        this.page = page + 1
                        this.getVideos()
                        $('html, body').animate({ scrollTop: $('.video-list').offset().top }, 1000)
                    },
                    ajaxSetup() {
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });
                    },
                    getVideos() {
                        var self = this
                        $.post('get-videos', {
                            page: self.page,
                            limit: 12
                        }).done(function(data) {
                            self.videos = data.result
                        })
                    },
                    initSlider() {
                        var owltop = $('.owl-top');
                        owltop.owlCarousel({
                            loop: true,
                            margin: 30,
                            nav: false,
                            dots: true,
                            singleItem: true,
                            responsive: {
                                0: {
                                    items: 1
                                },
                                600: {
                                    items: 1
                                },
                                1000: {
                                    items: 1
                                }
                            }
                        });

                        $('header .btn-nav-close').on('click', function(){
                            $('body').removeClass('show-nav');
                        });
                        $('header .navbar-toggler').on('click', function(){
                            $('body').addClass('show-nav');
                        });
                    },
                    initLink() {
                        $('#cara-ikutan').on('click', function() {
                            $('html, body').animate({ scrollTop: $('.cara_ikutan').offset().top }, 1000)
                        })

                        $('#syarat-ketentuan').on('click', function() {
                            $('#modal-syarat-ketentuan').modal('show')
                        })

                        $('#video-one-heart').on('click', function() {
                            $('html, body').animate({ scrollTop: $('.video-list').offset().top }, 1000)
                        })
                    },
                    initScroll() {
                        var topHeight = $(window).scrollTop();
                        if (topHeight > 50) {
                            $('header').addClass("scroll");
                        } else {
                            $('header').removeClass("scroll");
                        }

                        $(window).scroll(function(event) {
                            var topHeight = $(window).scrollTop();
                            if (topHeight > 50) {
                                $('header').addClass("scroll");
                            } else {
                                $('header').removeClass("scroll");
                            }
                        });
                    }
                },
                mounted() {
                    this.ajaxSetup()
                    this.getVideos()
                    this.initSlider()
                    this.initLink()
                    this.initScroll()
                    this.initModal()
                }
            })
        </script>
        <!-- Facebook Pixel Code -->
        <script>
          !function(f,b,e,v,n,t,s)
          {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
          n.callMethod.apply(n,arguments):n.queue.push(arguments)};
          if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
          n.queue=[];t=b.createElement(e);t.async=!0;
          t.src=v;s=b.getElementsByTagName(e)[0];
          s.parentNode.insertBefore(t,s)}(window, document,'script',
          'https://connect.facebook.net/en_US/fbevents.js');
          fbq('init', '1789265251316436');
          fbq('track', 'PageView');
        </script>
        <noscript><img height="1" width="1" style="display:none"
          src="https://www.facebook.com/tr?id=1789265251316436&ev=PageView&noscript=1"
        /></noscript>
        <!-- End Facebook Pixel Code -->

        <!-- Global site tag (gtag.js) - Google Analytics -->

        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-124183065-1"></script>

        <script>

          window.dataLayer = window.dataLayer || [];

          function gtag(){dataLayer.push(arguments);}

          gtag('js', new Date());

         

          gtag('config', 'UA-124183065-1');

        </script>
    </body>
</html>