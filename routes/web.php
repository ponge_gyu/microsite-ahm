<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::post('auth/login', 'AuthController@login')->name('auth-login');
Route::post('get-videos', 'VideoController@getVideos')->name('get-videos');

Route::group(['middleware' => 'verify.api.token'], function() {

	// Video Routes
	Route::post('video/get-videos', 'VideoController@getVideos')->name('get-videos');
	Route::post('video/get-all-videos', 'VideoController@getAllVideos')->name('get-all-videos');
	Route::post('video/get-single-video', 'VideoController@getSingleVideo')->name('get-single-video');
	Route::post('video/create', 'VideoController@createVideo')->name('create-video');
	Route::post('video/update', 'VideoController@updateVideo')->name('update-video');
	Route::post('video/delete', 'VideoController@deleteVideo')->name('delete-video');
});

Route::get('{path}', function() {
	if (strpos(url()->current(), '/admin')) {
		return view('app');
	} else {
		return view('landing');
	}
})->where('path', '.*');
